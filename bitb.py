#! /usr/bin/python
# -*- coding: utf-8 -*-

#MiniArvuti
#Autor: Karl-Erik Krais
#Kirjeldus: Skript käivitab "mini-arvuti", millega saab täita ülesandeid: 1) Kalkulaator (+,-,*,/) 2) Valuuta teisendamine (EUR, GBP,) 3)Google otsing 4) Faili kirjutamine

#Versioon: 0.6
#1 GBP = 1.26790 EUR
#1 EUR = 0.788704 GBP

import webbrowser
from urllib import quote
import time

#                       PÕHI FUNKTSIOONID
#-------------------------------------------------------------------------

# Kui valiti 1) Kalkulaator
def kalkulaator():
    
#Küsime, mida soovitakse teha: 1)+ 2)- 3)* 4)/
#Valik muutujasse
    kasutajaValik = raw_input ("Mida soovid teha? 1) Liita 2) Lahutada 3) Korrutada 4) Jagada \n")
    #Kui valiti 1) Liitmine
    if kasutajaValik == "1":
        #Küsime esimese liidetava
        #Valik muutujasse
        esimeneLiidetav = checkArgument(raw_input ("Sisesta esimene liidetav: \n"))
        #Küsime teise liidetava
        #Valik muutujasse
        teineLiidetav = checkArgument(raw_input ("Sisesta teine liidetav: \n"))
        #Liitmine= esimene liidetav + teine liidetav
        liitmine = esimeneLiidetav + teineLiidetav
        #Kuvame vastuse
        print "Vastus = %s" % (liitmine)
        uuesti()
    
    #Kui valiti 2) Lahutamine
    elif kasutajaValik == "2":
        #Küsime vähendatava
        #Valik muutujasse
        v2hendatav = checkArgument(raw_input ("Sisesta vähendatav: \n"))
        #Küsime vähendaja
        #Valik muutujasse
        v2hendaja= checkArgument(raw_input ("Sisesta vähendaja: \n"))
        #Lahutamine= Vähendatav - Vähendaja
        lahutamine = v2hendatav - v2hendaja
        #Kuvame vastuse
        print "Vastus = %s" % (lahutamine)
        uuesti()
    
    #Kui valiti 3) Korrutamine
    elif kasutajaValik == "3":
        #Küsime esimese teguri
        #Valik muutujasse
        tegur1 = checkArgument(raw_input ("Sisesta esimene tegur: \n"))
        #Küsime teise teguri
        #Valik muutujasse
        tegur2 = checkArgument(raw_input ("Sisesta teine tegur: \n"))
        #Korrutis= esimene tegur * teine tegur
        korrutis = tegur1 * tegur2
        #Kuvame vastuse
        print "Vastus = %s" % (korrutis)
        uuesti()
    
    #Kui valiti 4) Jagamine
    elif kasutajaValik == "4":
        #Küsime jagatava
        #Valik muutujasse
        jagatav = checkArgument(raw_input ("Sisesta jagatav: \n"))
        #Küsime jagaja
        #Valik muutujasse
        jagaja = checkArgument(raw_input ("Sisesta jagaja: \n"))
        #Jagatis= jagatav / jagaja
        jagatis = jagatav / jagaja
        #Kuvame vastuse
        print "Vastus = %s" % (jagatis)
        uuesti()

    #Midagi valesti
    else:
        print "Sisestasid midagi valesti"
        uuesti()

# Kui valiti 2) Valuuta

def valuutaTeisendaja():

#Küsime, mida soovitakse teisendada: 1) EUR->GBP 2) GBP->EUR
#Valik muutujasse

    kasutajaValik = raw_input ("Mida soovid teisendada? 1) EUR->GBP 2) GBP->EUR \n")
    #Kui valiti 1) EUR->GBP
    if kasutajaValik == "1":
        #Küsime summat,mida soovitakse teisendada
        #Valik muutujasse
        kasutajaEUR = checkArgument(raw_input ("Sisesta summa mida soovid teisendada \n"))
        print kasutajaEUR
        #GBP = valitud EUR * 0,788704
        naela = float(kasutajaEUR) * float(0.788704)
        #Kuvame vastuse
        naela= round(naela, 2)
        print "EUR %s = %s naela" % (kasutajaEUR, naela)
        uuesti()
    
    #Kui valiti 2) GBP->EUR
    elif kasutajaValik == "2":
        
        #Küsime summat,mida soovitakse teisendada
        #Valik muutujasse
        kasutajaGBP = checkArgument(raw_input ("Sisesta summa mida soovid teisendada \n"))
        #EUR = valitud GBP * 1.26790
        eurot = float(kasutajaGBP) * float(1.26790)
        #Kuvame vastuse
        eurot= round(eurot, 2)
        print "GBP %s = %s eurot" % (kasutajaGBP, eurot)
        uuesti()

    #Kui midagi läks valesti
    else:
        print "Sisestasid midagi valesti"
        uuesti()


# Kui valiti 3) Google otsing
def googleSearch():

    #Küsime mida soovitakse otsida
    kasutajaValik = raw_input("Sisesta otsitav termin: \n") 
    #Google otsinguleht
    baseURL = "http://www.google.com/?#q="
    #Otsime sisestatud terminit Google'st
    final_url = baseURL + quote(kasutajaValik)
    #Avame uue tab-i
    webbrowser.get('firefox').open(final_url)
    uuesti()
    
# Kui valiti 4) Faili kirjutamine
def kirjuta():

    kasutajaValik = raw_input ("1) Soovin kirjutada 2) Soovin teha midagi muud \n")
    if kasutajaValik == "1":
        pealkiri = raw_input ("Anna failile nimi: \n")
        tekst = raw_input ("Sisesta soovitav tekst: \n")
        file = open(pealkiri, "w")
        file.write(tekst)
        file.close
        arvuti()
        
    elif kasutajaValik == "2":
        arvuti()
    else:
        print "error"
#------------------------------------------------------------    
#Skripti uuesti jooksutamine
def uuesti():
    kasutajaValik = raw_input ("Mida soovid teha: 1) Jätkan 2) Sulge kõik \n")
    if kasutajaValik == "1":
        arvuti()
    elif kasutajaValik == "2":
        print "Bye bye!"
    else:
        print "Midagi läks valesti"
             
        
#------------------------------------------------------
# Uurime väja, mida kasutaja tahab teha: 1) Kalkulaator 2) Valuuta teisendus 3) Google otsing
# Kasutaja vastus muutujasse
def arvuti():
    kasutajaValik = raw_input ("Mida soovid kasutada? 1) Kalkulaator 2) Valuuta teisendus 3) Google otsing 4) Tekst faili kirjutamine \n")

    if kasutajaValik == "1":
        kalkulaator()
    elif kasutajaValik == "2":
        valuutaTeisendaja()
    elif kasutajaValik == "3":
        googleSearch()
    elif kasutajaValik == "4":
        kirjuta()
    else:
        print "Midagi läks valesti"
        arvuti()
        
#Argumendi kontroll
def checkArgument(number):
    return_value = None
    try:
        return_value = float(number)
    except Exception:
        return checkArgument(raw_input("Vale sisestus; Sisesta number \n"))
    return return_value    
        
#MiniPC käivitus
time.sleep(2.0)
print "MiniPC käivitub..."
time.sleep(2.0)
print "Valmis!"
arvuti()



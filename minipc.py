#! /usr/bin/python
# -*- coding: utf-8 -*-

#MiniArvuti
#Autor: Karl-Erik Krais
#Kirjeldus: Skript käivitab "mini-arvuti", millega saab täita ülesandeid: 1) Kalkulaator (+,-,*,/) 2) Valuuta teisendamine (EUR, GBP,) 3)Google otsing

#Versioon: 0.4
#1 GBP = 1.26790 EUR
#1 EUR = 0.788704 GBP
#-------------------------------------------------------------------------

# Uurime väja, mida kasutaja tahab teha: 1) Kalkulaator 2) Valuuta teisendus 3) Google otsing

# Kasutaja vastus muutujasse

# Kui valiti 1) Kalkulaator
def kalkulaator():

#Küsime, mida soovitakse teha: 1)+ 2)- 3)* 4)/
#Valik muutujasse
kasutajaValik = raw_input ("Mida soovid teha? 1) Liita 2) Lahutada 3) Korrutada 4) Jagada \n")
    #Kui valiti 1) Liitmine
    if kasutajaValik == "1":
        #Küsime esimese liidetava
        #Valik muutujasse
        esimeneLiidetav = input ("Sisesta esimene liidetav: \n")
        print esimeneLiidetav
        #Küsime teise liidetava
        #Valik muutujasse
        teineLiidetav = input ("Sisesta teine liidetav: \n")
        #Liitmine= esimene liidetav + teine liidetav
        liitmine = esimeneLiidetav + teineLiidetav
        #Kuvame vastuse
        print liitmine
    
    #Kui valiti 2) Lahutamine
    elif kasutajaValik == "2":
        #Küsime vähendatava
        #Valik muutujasse
        v2hendatav = input ("Sisesta vähendatav: \n")
        #Küsime vähendaja
        #Valik muutujasse
        v2hendaja= input ("Sisesta vähendaja: \n")
        #Lahutamine= Vähendatav - Vähendaja
        lahutamine = v2hendatav + v2hendaja
        #Kuvame vastuse
        print lahutamine
    
    #Kui valiti 3) Korrutamine
    elif kasutajaValik == "3":
        #Küsime esimese teguri
        #Valik muutujasse
        tegur1 = input ("Sisesta esimene tegur: \n")
        #Küsime teise teguri
        #Valik muutujasse
        tegur2 = input ("Sisesta teine tegur: \n")
        #Korrutis= esimene tegur * teine tegur
        korrutis = tegur1 * tegur2
        #Kuvame vastuse
        print korrutis
    
    #Kui valiti 4) Jagamine
    elif kasutajaValik == "4":
        #Küsime jagatava
        #Valik muutujasse
        jagatav = input ("Sisesta jagatav: \n")
        #Küsime jagaja
        #Valik muutujasse
        jagaja = input ("Sisesta jagaja: \n")
        #Jagatis= jagatav / jagaja
        jagatis = jagatav / jagaja
        #Kuvame vastuse
        print jagatis

    #Midagi valesti
    else:
        print "Sisestasid midagi valesti"

# Kui valiti 2) Valuuta

def valuutaTeisendaja():



#Küsime, mida soovitakse teisendada: 1) EUR->GBP 2) GBP->EUR
#Valik muutujasse

    kasutajaValik = raw_input ("Mida soovid teisendada? 1) EUR->GBP 2) GBP->EUR \n")
    #Kui valiti 1) EUR->GBP
    if kasutajaValik == "1":
        #Küsime summat,mida soovitakse teisendada
        #Valik muutujasse
        kasutajaEUR = input ("Sisesta summa mida soovid teisendada \n")
        #GBP = valitud EUR * 0,788704
        naela = kasutajaEUR * 0.788704
        #Kuvame vastuse
        naela= round(naela, 2)
        print "EUR %s = %s naela" % (kasutajaEUR, naela)
    
    #Kui valiti 2) GBP->EUR
    elif kasutajaValik == "2":
        
        #Küsime summat,mida soovitakse teisendada
        #Valik muutujasse
        kasutajaGBP = input ("Sisesta summa mida soovid teisendada \n")
        #EUR = valitud GBP * 1.26790
        eurot = kasutajaGBP * 1.26790
        #Kuvame vastuse
        eurot= round(eurot, 2)
        print "GBP %s = %s eurot" % (kasutajaGBP, eurot)

    #Kui midagi läks valesti
        else:
            print "Sisestasid midagi valesti"


# Kui valiti 3) Google otsing
#



kalkulaator()
